package org.jslain.osgi.socket.api;

import java.io.IOException;

public interface IServerSocketFactory {

	IServerSocketController createServerSocket(int port, ISocketHandler handler) throws IOException;
}
