package org.jslain.osgi.socket.api;

import java.net.Socket;

public interface ISocketHandler {

	void onConnect(Socket socket);
	
	void onTerminated();
	
	void onError(Exception e);
}
