package org.jslain.osgi.socket.api;

import java.io.IOException;

public interface IServerSocketController {

	void configure(int port, ISocketHandler handler) throws IOException;
	
	void start();
	
	void stop() throws IOException;
}
