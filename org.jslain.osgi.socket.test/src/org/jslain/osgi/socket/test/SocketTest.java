package org.jslain.osgi.socket.test;

import java.net.Socket;

import org.jslain.osgi.socket.api.IServerSocketFactory;
import org.jslain.osgi.socket.api.ISocketHandler;
import org.junit.Assert;
import org.junit.Test;
import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;
import org.osgi.util.tracker.ServiceTracker;



public class SocketTest {

    private final BundleContext context = FrameworkUtil.getBundle(this.getClass()).getBundleContext();
    
    @Test
    public void testSocket() throws Exception {
    	Assert.assertNotNull(context);
    	
    	IServerSocketFactory impl = getService(IServerSocketFactory.class);
    	Assert.assertNotNull(impl);
    	
    	ISocketHandler handler = new ISocketHandler(){
    		@Override
    		public void onConnect(Socket socket) {
    			System.out.println("Connected");
    		}
    		
    		@Override
    		public void onError(Exception e) {
    			System.out.println(e);
    		}
    		
    		@Override
    		public void onTerminated() {
    			System.out.println("Terminated!");
    		}
    	};
    	
    	impl.createServerSocket(12345, handler);
    	
    	System.out.println("Terminé!");
    }
    
    <T> T getService(Class<T> clazz) throws InterruptedException {
    	ServiceTracker<T,T> st = new ServiceTracker<>(context, clazz, null);
    	st.open();
    	return st.waitForService(1000);
    }
}
