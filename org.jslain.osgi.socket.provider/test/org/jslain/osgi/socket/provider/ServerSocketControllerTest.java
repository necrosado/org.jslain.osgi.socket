package org.jslain.osgi.socket.provider;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.jslain.osgi.socket.api.ISocketHandler;
import org.junit.Before;
import org.junit.Test;


public class ServerSocketControllerTest {

	
	
	private ServerSocketController underTest;
	private ISocketHandler mockSocketHandler;
	private ServerSocketImpl mockServerSocketImpl;

	@Before
	public void setup_underTest(){
		mockSocketHandler = mock(ISocketHandler.class);
		mockServerSocketImpl = mock(ServerSocketImpl.class);

		underTest = new ServerSocketController(mockServerSocketImpl);
		
	}
	
	@Test
	public void unconfigured_whenConfigure_rightValuesAreKept() throws Exception{
		
		underTest.configure(1234, mockSocketHandler);
		
		assertEquals(1234, underTest.getPort());
		assertSame(mockSocketHandler, underTest.getHandler());
	}
	
	@Test
	public void started_whenReconfigured_handlerIsStoppedAndRestarted() throws Exception{
		when(mockServerSocketImpl.isStarted()).thenReturn(true);
		
		underTest.configure(12345, mockSocketHandler);
		
		verify(mockServerSocketImpl).stop();
		verify(mockServerSocketImpl).start(12345, mockSocketHandler);
	}
	
	@Test
	public void whenStarted_serverSocketGetStarted() throws Exception{
		underTest.configure(1234, mockSocketHandler);
		underTest.start();
		
		verify(mockServerSocketImpl).start(1234, mockSocketHandler);
	}
	
	@Test(expected=IllegalStateException.class)
	public void alreadyStarted_whenStarted_anExceptionIsThrown() throws Exception{
		when(mockServerSocketImpl.isStarted()).thenReturn(true);
		
		underTest.configure(1234, mockSocketHandler);
		underTest.start();
	}
	
	@Test
	public void alreadyStarted_whenStopped_serverSocketGetStopped() throws Exception{
		underTest.stop();
		
		verify(mockServerSocketImpl).stop();
	}
}
