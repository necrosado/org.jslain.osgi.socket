package org.jslain.osgi.socket.provider;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.jslain.osgi.socket.api.ISocketHandler;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

public class ServerSocketImplTest {

	
	private ServerSocketImpl underTest;
	private ISocketHandler socketHandler;
	private ExecutorService executorService;

	public ServerSocketImplTest() {
		// TODO Auto-generated constructor stub
	}
	
	@Before
	public void setupUnderTest() {
		executorService = Executors.newSingleThreadExecutor();
		
		underTest = new ServerSocketImpl(executorService);

		socketHandler = mock(ISocketHandler.class);
	}

	@After
	public void tearDownUnderTest() {
		Future<?> future = underTest.getFuture();
		if (future != null) {
			future.cancel(true);
		}
	}

	@Test
	public void clean_whenStarted_theRightVariablesAreKept() throws Exception{
		underTest.start(1234, socketHandler);

		assertEquals(1234, underTest.getPort());
		assertSame(socketHandler, underTest.getSocketHandler());
	}

	@Test
	public void clean_whenStarted_threadIsStarted() throws Exception{
		underTest.start(1234, socketHandler);

		Future<?> future = underTest.getFuture();

		assertNotNull(future);
		assertTrue("Future should be running", !future.isDone());
	}

	@Test
	public void started_whenStopped_futureIsCancelled() throws Exception {
		underTest.start(1234, socketHandler);

		Future<?> future = underTest.getFuture();

		assertFalse("Future shouldn't be cancelled", future.isCancelled());
		assertFalse("Future shouldn't be finished", future.isDone());

		underTest.stop();

		Thread.sleep(100L);

		assertTrue("Thread shouldn't be running", future.isCancelled());
	}

	@Test
	public void whenFutureIsNotDone_isStartedShouldReturnTrue() {
		Future<?> future = mock(Future.class);

		when(future.isDone()).thenReturn(false);

		underTest.setFuture(future);
	}
	
	@Test
	public void whenStarted_serverSocketStarted() throws Exception{
		ServerSocketFactory mockServerSocketFactory = mock(ServerSocketFactory.class);
		
		underTest.setServerSocketFactory(mockServerSocketFactory);
		
		underTest.start(1234, socketHandler);
		
		Thread.sleep(100L);
		
		verify(mockServerSocketFactory).create(1234);
	}
	
	@Test
	public void started_whenIOExceptionIsThrown_handlerGetNotifiedAboutTheError() throws Exception{
		ServerSocketFactory mockServerSocketFactory = mock(ServerSocketFactory.class);
		
		underTest.setServerSocketFactory(mockServerSocketFactory);
		
		Exception error = new IOException();
		when(mockServerSocketFactory.create(1234)).thenThrow(error);
		
		underTest.start(1234, socketHandler);

		Thread.sleep(100L);
		
		verify(socketHandler).onError(error);
	}
	
	@Test
	public void started_whenReceivingSocketInput_handlerGetNotified() throws Exception{
		ServerSocket mockServerSocket = mockServerSocket();
		final Socket mockSocket = mock(Socket.class);
		
		when(mockServerSocket.accept()).then(new Answer<Socket>(){
			@Override
			public Socket answer(InvocationOnMock invocation) throws Throwable {
				underTest.getFuture().cancel(false);
				return mockSocket;
			}
		});
		
		underTest.start(1234, socketHandler);
		
		Thread.sleep(200L);
		
		verify(socketHandler).onConnect(mockSocket);
	}
	
	@Test
	public void started_whenStop_serverSocketShouldBeStopped() throws Exception{
		ServerSocket mockServerSocket = mockServerSocket();
		
		underTest.start(1234, socketHandler);
		
		assertTrue(underTest.isStarted());

		Thread.sleep(200L);
		
		underTest.stop();
		
		verify(mockServerSocket).close();
		assertFalse(underTest.isStarted());
	}

	private ServerSocket mockServerSocket() throws IOException {
		ServerSocketFactory mockServerSocketFactory = mock(ServerSocketFactory.class);
		ServerSocket mockServerSocket = mock(ServerSocket.class);
		when(mockServerSocketFactory.create(1234)).thenReturn(mockServerSocket);
		underTest.setServerSocketFactory(mockServerSocketFactory);
		
		return mockServerSocket;
	}
}
