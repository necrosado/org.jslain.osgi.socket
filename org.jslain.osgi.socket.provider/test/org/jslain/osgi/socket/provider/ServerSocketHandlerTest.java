package org.jslain.osgi.socket.provider;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Map;
import java.util.concurrent.ExecutorService;

import org.jslain.osgi.socket.api.IServerSocketController;
import org.jslain.osgi.socket.api.ISocketHandler;
import org.junit.Before;
import org.junit.Test;


public class ServerSocketHandlerTest {

	private ServerSocketImplFactory mockFactory;
	private ServerSocketHandler underTest;
	private Map<Integer, ServerSocketController> servers;
	private ServerSocketImpl mockServer;
	private ExecutorServiceFactory mockExecutorServiceFactory;
	private ExecutorService mockExecutorService;

	@Before
	public void setupUnderTest(){
		underTest = new ServerSocketHandler();
		underTest.activate();
		
		mockExecutorServiceFactory = mock(ExecutorServiceFactory.class);
		mockFactory = mock(ServerSocketImplFactory.class);
		underTest.setExecutorServiceFactory(mockExecutorServiceFactory);
		underTest.setServerSocketFactory(mockFactory);
		
		mockExecutorService = mock(ExecutorService.class);
		mockServer = mock(ServerSocketImpl.class);
		
		when(mockExecutorServiceFactory.create()).thenReturn(mockExecutorService);
		when(mockFactory.create(mockExecutorService)).thenReturn(mockServer);
		
		servers = underTest.getServerSockets();
	}
	
	@Test
	public void notCreated_whenCreate_serverSocketIsCreated() throws Exception{
		ISocketHandler mockHandler = mock(ISocketHandler.class);

		IServerSocketController serverSocketController = underTest.createServerSocket(1234, mockHandler);
		serverSocketController.start();
		
		assertEquals(1, servers.size());
		verify(mockServer).start(1234, mockHandler);
	}
	
	@Test(expected=IllegalStateException.class)
	public void alreadyExistingWithSamePort_whenCreatedAgain_exceptionIsThrown() throws Exception{
		servers.put(1234, new ServerSocketController(new ServerSocketImpl(mockExecutorService)));
		
		underTest.createServerSocket(1234, null);
	}
	
	@Test
	public void notCreated_whenCreateTwoServersUsingDifferentPorts_twoServersAreCreated() throws Exception{
		underTest.createServerSocket(1234, null);
		underTest.createServerSocket(2345, null);
		
		assertEquals(2, servers.size());
	}
	
	@Test
	public void existingServers_whenDeactivated_serversAreStopped() throws Exception{
		servers.put(1234, new ServerSocketController(mockServer));
		when(mockServer.isStarted()).thenReturn(true);
		
		underTest.deactivate();
		
		verify(mockServer).stop();
	}
	
	@Test
	public void existingServers_whenDeactivated_mapOfServersIsCleared() throws Exception{
		servers.put(1234, new ServerSocketController(mockServer));
		
		underTest.deactivate();
		
		assertTrue("Servers map should be empty after deactivation.", servers.isEmpty());
	}
}
