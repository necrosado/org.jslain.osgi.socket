package org.jslain.osgi.socket.provider;

import static org.junit.Assert.assertNotNull;

import java.util.concurrent.ExecutorService;

import org.junit.Test;

public class ExecutorServiceFactoryTest {

	@Test
	public void whenCreate_SingleThreadExecutorIsCreated(){
		ExecutorServiceFactory underTest = new ExecutorServiceFactory();
		
		ExecutorService result = underTest.create();
		
		assertNotNull("Created executor shouldn't be null", result);
	}
}
