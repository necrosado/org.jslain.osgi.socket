package org.jslain.osgi.socket.provider;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.net.ServerSocket;

import org.junit.After;
import org.junit.Test;


public class ServerSocketFactoryTest {
	
	ServerSocket serverSocket;
	
	@After
	public void teardownServer() throws Exception{
		if(serverSocket != null){
			serverSocket.close();
		}
	}
	
	@Test
	public void whenCreate_serverSocketIsCreated() throws Exception{
		ServerSocketFactory serverSocketFactory = new ServerSocketFactory();
		
		serverSocket = serverSocketFactory.create(1234);
		
		assertNotNull(serverSocket);
		assertEquals(1234, serverSocket.getLocalPort());
	}
}
