package org.jslain.osgi.socket.provider;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;

import java.util.concurrent.ExecutorService;

import org.junit.Test;


public class ServerSocketImplFactoryTest {

	@Test
	public void create(){
		ExecutorService executorService = mock(ExecutorService.class);
		
		ServerSocketImplFactory serverSocketFactory = new ServerSocketImplFactory();
		
		ServerSocketImpl created = serverSocketFactory.create(executorService );
		assertNotNull(created);
	}
}
