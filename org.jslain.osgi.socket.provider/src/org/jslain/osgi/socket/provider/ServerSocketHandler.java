package org.jslain.osgi.socket.provider;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;

import org.jslain.osgi.socket.api.IServerSocketController;
import org.jslain.osgi.socket.api.IServerSocketFactory;
import org.jslain.osgi.socket.api.ISocketHandler;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Deactivate;


@Component(name="org.jslain.osgi.socket",
		servicefactory=true
		)
public class ServerSocketHandler implements IServerSocketFactory{

	private ExecutorServiceFactory executorServiceFactory;
	private ServerSocketImplFactory serverSocketFactory;
	private Map<Integer, ServerSocketController> serverSockets;
	
	@Activate
	public void activate(){
		setExecutorServiceFactory(new ExecutorServiceFactory());
		setServerSocketFactory(new ServerSocketImplFactory());
		serverSockets = new HashMap<>();
	}
	
	@Deactivate
	public void deactivate() throws Exception{
		for(ServerSocketController server : serverSockets.values()){
			server.stop();
		}
		serverSockets.clear();
	}
	
	@Override
	public IServerSocketController createServerSocket(int port, ISocketHandler handler) throws IOException{
		if(serverSockets.containsKey(port)){
			throw new IllegalStateException("Can't create socket, port " + port + "is already used");
		}
		
		ExecutorService executorService = executorServiceFactory.create();
		ServerSocketImpl serverSocketImpl = getServerSocketFactory().create(executorService);
		ServerSocketController serverSocketController = new ServerSocketController(serverSocketImpl);
		serverSocketController.configure(port, handler);
		
		serverSockets.put(new Integer(port), serverSocketController);
		
		return serverSocketController;
	}

	protected Map<Integer, ServerSocketController> getServerSockets() {
		return serverSockets;
	}

	protected ServerSocketImplFactory getServerSocketFactory() {
		return serverSocketFactory;
	}

	protected void setServerSocketFactory(ServerSocketImplFactory serverSocketFactory) {
		this.serverSocketFactory = serverSocketFactory;
	}

	public ExecutorServiceFactory getExecutorServiceFactory() {
		return executorServiceFactory;
	}

	public void setExecutorServiceFactory(ExecutorServiceFactory executorServiceFactory) {
		this.executorServiceFactory = executorServiceFactory;
	}

}
