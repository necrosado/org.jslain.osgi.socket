package org.jslain.osgi.socket.provider;

import java.util.concurrent.ExecutorService;

public class ServerSocketImplFactory {

	public ServerSocketImpl create(ExecutorService executorService){
		return new ServerSocketImpl(executorService);
	}
}
