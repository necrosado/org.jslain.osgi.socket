package org.jslain.osgi.socket.provider;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

import org.jslain.osgi.socket.api.ISocketHandler;

public class ServerSocketImpl implements Runnable{

	private int port;
	private ISocketHandler socketHandler;
	
	//Socket
	private ServerSocketFactory serverSocketFactory;
	
	//Thread
	private Future<?> future;
	private ExecutorService executorService;
	private ServerSocket serverSocket;
	
	public ServerSocketImpl(ExecutorService executorService){
		this.setExecutorService(executorService);
		setServerSocketFactory(new ServerSocketFactory());
	}
	
	@Override
	public void run() {
		try{
			serverSocket = getServerSocketFactory().create(getPort());
			
			while(!getFuture().isCancelled()){
				Socket socket = serverSocket.accept();
				
				socketHandler.onConnect(socket);
				
				socket.close();
			}
		}catch(IOException e){
			socketHandler.onError(e);
		}
	}
	
	public void start(int port, ISocketHandler handler){
		this.port = port;
		this.socketHandler = handler;
		
		this.setFuture(getExecutorService().submit(this));
	}
	
	public boolean isStarted(){
		return getFuture() != null && !getFuture().isDone() && !getFuture().isCancelled();
	}

	public void stop() throws IOException{
		getFuture().cancel(true);
		if(serverSocket != null){
			serverSocket.close();
		}
	}

	protected int getPort() {
		return port;
	}

	
	protected ISocketHandler getSocketHandler() {
		return socketHandler;
	}

	public Future<?> getFuture() {
		return future;
	}

	public void setFuture(Future<?> future) {
		this.future = future;
	}

	public ExecutorService getExecutorService() {
		return executorService;
	}

	public void setExecutorService(ExecutorService executorService) {
		this.executorService = executorService;
	}

	public ServerSocketFactory getServerSocketFactory() {
		return serverSocketFactory;
	}

	public void setServerSocketFactory(ServerSocketFactory serverSocketFactory) {
		this.serverSocketFactory = serverSocketFactory;
	}
}
