package org.jslain.osgi.socket.provider;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ExecutorServiceFactory {

	ExecutorService create(){
		return Executors.newSingleThreadExecutor();
	}
}
