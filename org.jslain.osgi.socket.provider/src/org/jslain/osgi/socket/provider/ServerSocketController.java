package org.jslain.osgi.socket.provider;

import java.io.IOException;

import org.jslain.osgi.socket.api.IServerSocketController;
import org.jslain.osgi.socket.api.ISocketHandler;

public class ServerSocketController implements IServerSocketController {

	private int port;
	private ISocketHandler handler;
	

	private ServerSocketImpl serverSocketImpl;
	
	public ServerSocketController(ServerSocketImpl serverSocketImpl) {
		this.serverSocketImpl = serverSocketImpl;
	}
	
	@Override
	public void configure(int port, ISocketHandler handler) throws IOException{
		boolean started = serverSocketImpl.isStarted();
		
		if(started){
			serverSocketImpl.stop();
		}
		
		this.port = port;
		this.handler = handler;
		
		if(started){
			serverSocketImpl.start(getPort(), getHandler());
		}
	}

	@Override
	public void start() {
		if(serverSocketImpl.isStarted()){
			throw new IllegalStateException("ServerSocket is already started.");
		}
		
		serverSocketImpl.start(port, handler);
	}

	@Override
	public void stop() throws IOException{
		serverSocketImpl.stop();
	}

	public int getPort() {
		return port;
	}
	
	public ISocketHandler getHandler() {
		return handler;
	}
	

}
