package org.jslain.osgi.socket.provider;

import java.io.IOException;
import java.net.ServerSocket;

public class ServerSocketFactory {

	public ServerSocket create(int port) throws IOException{
		return new ServerSocket(port);
	}
}
