# README #

STILL IN DEVELOPMENT, NOT READY TO USE!

This OSGI bundle offers a service for managing a simple ServerSocket lifecycle.
The client of this service only have to manage what's done when they receives a Socket. This is generally what it's interested in.

Quick example:

```
#!java

public class Example {

	private IServerSocketFactory serverSocketFactory;

	
	public void activated(){
		ISocketHandler myHandler = new ISocketHandler(){
			@Override
			public void onConnect(Socket socket) {
				System.out.println("Socket received!");
				
				/* Do whatever you want with that socket... */
			}
			
			@Override
			public void onError(Exception e) {
				System.out.println("Error! " + e);
			}
			
			@Override
			public void onTerminated() {
				System.out.println("Terminated!");
			}
		};
		
		getServerSocketFactory().createServerSocket(1234, myHandler);
	}
	
	@Reference
	public IServerSocketFactory getServerSocketFactory() {
		return serverSocketFactory;
	}

	public void setServerSocketFactory(IServerSocketFactory serverSocketFactory) {
		this.serverSocketFactory = serverSocketFactory;
	}	
}
```


### What is this repository for? ###

* I had a project to do that requires the use of a ServerSocket listening for requests. I decided to move that part out into its own bundle to be reused by anybody with the same needs.
* 0.0.1
* Dependency: OSGI component service.

### How do I get set up? ###

* Setup
I'm using Bndtools, and this project contains the Eclipse project.
There's nothing special about the configuration, everything is in Bndtools Hub.

* How to run tests
Tests are basics junit tests.

* Deployment instructions
Just add the bundle into your OSGI framework, and everything's fine. The only dependency is the OSGI Component Service bundle.

### Contribution guidelines ###

* Writing tests
* Don't break anything... ;)

### Who do I talk to? ###

* Repo owner or admin


### Sorry for the name ###
Since i made this bundle for myself, i put my nickname in the package. I was out of imagination, and will change it if someone find something better... I don't have a company and i'm not part of any open source association.